import { Http, Request, RequestOptions, RequestOptionsArgs, Response, XHRBackend } from "@angular/http"
import { Injectable } from "@angular/core"
import { Observable } from "rxjs/Rx"
import {LoadingController, Loading} from 'ionic-angular'

// operators
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/map";

@Injectable()
export class HttpInterceptor extends Http {

  // public loader;
  constructor(backend: XHRBackend, options: RequestOptions, public http: Http, public loadingCtrl: LoadingController ) {
    super(backend, options);
  }

  public request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    var loader: Loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();
    return super.request(url, options)
      // .catch(error => this.handleError(error))
      .catch(error => {return Observable.throw(JSON.parse(error._body));})
      .finally(() => this.handleFinal(loader))
  }

  public handleSubscriber  = (error: Response | any) => {
    console.log(error)
  };

  public handleError = (error: Response | any) => {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }

  public handleFinal = (loader: Loading) => {
    loader.dismiss();
  }
}
