import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {CategoryService} from "./category.service";
import {CategoryModal} from "./category.modal";
import { AlertController } from 'ionic-angular';
import { SingleCategoryPage } from '../../pages/single-category/single-category';
import { PopoverController } from 'ionic-angular';
import { MyPopOverPage } from '../my-category-popover/my-category-popover';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { DomSanitizer } from '@angular/platform-browser';

@IonicPage()
@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
})
export class CategoryPage {
  error;
  categoryList = [];
  offerList = [];
  childList;
  specificCategory = 'false';
  selectedCategoryName;
  googleMapsURL;
  public categoryModal = new CategoryModal;
  constructor(public popoverCtrl: PopoverController,private toastCtrl: ToastController,private alertCtrl: AlertController,
               public navCtrl: NavController,
              public categoryService: CategoryService, public navParams: NavParams,private iab: InAppBrowser,
              private sanitizer: DomSanitizer) {
    this.getCategory();
  }

  singleCategory(offerId, offerType) {
    console.log(offerType);
    this.navCtrl.push('SingleCategoryPage',{'offerId':offerId, 'offerType':offerType});
  }
  presentAlert(offer) {
    let alert = this.alertCtrl.create({
      title: 'Email',
      subTitle: offer.des.email
    });
    alert.present();
  }
  getCategory(){
    this.categoryService.getCategory().subscribe(response => {
      this.categoryList = response.json();
      // this.childList = response.json().childCategory;
      console.log(this.categoryList);
    }, error => {
      console.log(error);
      // this.error = JSON.parse(error._body);
    });
  }
  getSpecificCategory(){
    console.log(this.categoryModal.specific_category);
    if(this.categoryModal.specific_category == 'false' ){
      this.categoryModal.offer_category = null;
    }
    this.categoryService.getSpecificCategory(this.categoryModal).subscribe(response => {
      // this.offerList.push(response.json());
      this.offerList = response.json();

      this.googleMapsURL = "https://www.google.com/maps/embed/v1/place?key=AIzaSyB_0uMeFv4grdPqOr3OI-euMK5gYmY9xGM&q={{offer.lat}},{{offer.lng}}";
      if(this.offerList.length == 0){
        this.presentToast();
      } else {
        for(var index = 0; index < this.offerList.length; ++index) {
          if(this.offerList[index].lat && this.offerList[index].lng && this.offerList[index].lat != "" && this.offerList[index].lng != "") {
            this.offerList[index].mapURL = this.mapUrl(this.offerList[index].lat, this.offerList[index].lng);
          }
        }
      }
      console.log(this.offerList);
    }, error => {
      console.log(error);
      // this.error = JSON.parse(error._body);
    });
  }
  changeCategory(){
    this.specificCategory = this.categoryModal.specific_category;
    console.log(this.specificCategory);
  }

  openWeb(offer){
    // var link = offer.des.website;
    // console.log(link.indexOf("http://"),'rjhfhr');
    // if (link.indexOf("http://") == -1 ) {
    //   window.open('http://' + offer.des.website);
    // }else{
    //   window.open(offer.des.website);
    // }
    var link = offer.des.website;
    console.log(link.indexOf("http://"),'rjhfhr');
    if (link.indexOf("http://") == -1 ) {
      this.iab.create('http://' + offer.des.website);
    }else{
      this.iab.create(offer.des.website);
    }

  }
  // openDialler(offer){
  //   this.callNumber.callNumber(offer.des.telephone, true)
  //     .then(() => console.log('Launched dialer!'))
  //     .catch(() => console.log('Error launching dialer'));
  // }
  openCall(offer){
    window.open('tel:' + offer.des.telephone, '_system');
  }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'No Data Found!!',
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  featureCategory(id){
    this.categoryModal.offer_category = id;
    this.categoryService.getfeatureCategory(this.categoryModal).subscribe(response => {
      // this.offerList.push(response.json());
      this.offerList = response.json();
      if(this.offerList.length == 0){
        this.presentToast();
      }
      for(var index = 0; index < this.offerList.length; ++index) {
        this.offerList[index].mapURL = this.mapUrl(this.offerList[index].lat, this.offerList[index].lng);
      }
      console.log(this.offerList);
    }, error => {
      console.log(error);
      // this.error = JSON.parse(error._body);
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoryPage');
  }


  ngAfterViewInit() {
    // this.loadMap();
  }
  presentPopover() {
    console.log('fesrf');
    let popover = this.popoverCtrl.create(MyPopOverPage);
    popover.present();
    popover.onDidDismiss((popoverData) => {
      console.log(popoverData);
      if(popoverData){
        this.categoryModal.offer_category =popoverData.id;
        this.selectedCategoryName =popoverData.name;
      }

    })
  }
  mapUrl(lat, lng) {
    var tempURL = "https://www.google.com/maps/embed/v1/place?key=AIzaSyB_0uMeFv4grdPqOr3OI-euMK5gYmY9xGM&q=" + lat + "," + lng;
    return this.sanitizer.bypassSecurityTrustResourceUrl(tempURL);
  }
}
