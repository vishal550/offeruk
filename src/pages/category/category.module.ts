import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoryPage } from './category';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@NgModule({
  declarations: [
    CategoryPage,
  ],
  imports: [
    IonicPageModule.forChild(CategoryPage),
  ],
  providers: [
    InAppBrowser
  ]
})
export class CategoryPageModule {}
