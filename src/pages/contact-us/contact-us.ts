import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {ContactService} from "./contact-us.service";
import {ContactModal} from "./contact-us.modal";

/**
 * Generated class for the ContactUsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contact-us',
  templateUrl: 'contact-us.html',
})
export class ContactUsPage {
  public contactModal = new ContactModal;
  error;
  errorMsg;
  constructor(private toastCtrl: ToastController,public navCtrl: NavController, public navParams: NavParams, private contactService:ContactService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactUsPage');
  }


  presentToast() {
    let toast = this.toastCtrl.create({
      message: this.errorMsg,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  postContact(){
    this.contactService.postContact(this.contactModal).subscribe(response => {
      console.log(response.json());
      this.errorMsg = response.json().message;
      this.presentToast();
    }, error => {
      console.log(error);
      // this.error = JSON.parse(error._body);
    });
  }
}
