import {Headers, Http, RequestOptions, Response, URLSearchParams} from '@angular/http';
import {config} from '../app.config';
import {Injectable} from "@angular/core";
import 'rxjs/add/operator/map';
import {HttpInterceptor} from "../Interceptor";

@Injectable()
export class ContactService {

  private postContactUrl = config.getEnvironmentVariable('endPoint') + 'api/contact_us.php';
  private headers = config.getHeader();

  constructor(private http: HttpInterceptor) {
  }

  postContact(data){
    let authHeader = new Headers();
    let params = new URLSearchParams();
    params.append('username', data.username);
    params.append('email', data.email);
    params.append('message', data.message);
    params.append('phone', data.phone);
    let options = new RequestOptions({ headers: authHeader, search: params });
    return this.http.get(this.postContactUrl,options);
  }

}





