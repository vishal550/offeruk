import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {SingleCategoryService} from './single-category.service';
import { AlertController } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
/**
 * Generated class for the SingleCategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-single-category',
  templateUrl: 'single-category.html',
})
export class SingleCategoryPage {
  offerId;
  offerType;
  singleInfo;
  offerSearch;
  constructor(private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public singleCategoryServie:SingleCategoryService,
              private sanitizer: DomSanitizer) {
    this.offerId = this.navParams.get('offerId');
    this.offerType = this.navParams.get('offerType');
    console.log(this.offerId);
    console.log(this.offerType);
    if(this.offerId){
      this.singleCategoryServie.getSingleCategory(this.offerId, this.offerType).map(response => response.json()).subscribe(response => {
          this.singleInfo = response;
          console.log(this.singleInfo);
          if(this.singleInfo.list_breif.map_latitude != "" && this.singleInfo.list_breif.map_longitude != "" && this.singleInfo.list_breif.map_latitude && this.singleInfo.list_breif.map_longitude)
            this.singleInfo.list_breif.mapURL = this.mapUrl(this.singleInfo.list_breif.map_latitude, this.singleInfo.list_breif.map_longitude);
          // }
        },
        error => {

        });
    }
    if(this.offerType == 'offer'){
      this.offerSearch = true;
    }
    else{
      this.offerSearch = false;

    }
  }
  presentAlert(singleInfo) {
    let alert = this.alertCtrl.create({
      title: 'Email',
      subTitle: singleInfo.list_breif.email
    });
    alert.present();
  }
  openWeb(singleInfo){
    var link = singleInfo.website;
    console.log(link.indexOf("http://"),'rjhfhr');
    if (link.indexOf("http://") == -1 ) {
      window.open('http://' + singleInfo.list_breif.website);
    }else{
      window.open(singleInfo.list_breif.website);
    }

  }
  // openDialler(singleInfo){
  //   this.callNumber.callNumber(singleInfo.list_breif.telephone, true)
  //     .then(() => console.log('Launched dialer!'))
  //     .catch(() => console.log('Error launching dialer'));
  // }
  openCall(singleInfo){
    window.open('tel:' + singleInfo.list_breif.telephon, '_system');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SingleCategoryPage');
  }
  mapUrl(lat, lng) {
    var tempURL = "https://www.google.com/maps/embed/v1/place?key=AIzaSyB_0uMeFv4grdPqOr3OI-euMK5gYmY9xGM&q=" + lat + "," + lng;
    return this.sanitizer.bypassSecurityTrustResourceUrl(tempURL);
  }
}
