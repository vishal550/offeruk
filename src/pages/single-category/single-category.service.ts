import {Headers, Http, RequestOptions, Response, URLSearchParams} from '@angular/http';
import {config} from '../app.config';
import {Injectable} from "@angular/core";
import 'rxjs/add/operator/map';
import {HttpInterceptor} from "../Interceptor";

@Injectable()
export class SingleCategoryService {

  private getSingleCategoryUrl = config.getEnvironmentVariable('endPoint') + 'api/listing_detail_service.php';
  private headers = config.getHeader();

  constructor(private http: HttpInterceptor) {}

  getSingleCategory(offerId,offerType){
    let authHeader = new Headers();
    let params = new URLSearchParams();
    params.append('listing_id', offerId);
    params.append('type', offerType);
    let options = new RequestOptions({ headers: authHeader, search: params });
    return this.http.get(this.getSingleCategoryUrl,options);
  }


}





