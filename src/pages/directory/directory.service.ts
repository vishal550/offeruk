import {Headers, Http, RequestOptions, Response, URLSearchParams} from '@angular/http';
import {config} from '../app.config';
import {Injectable} from "@angular/core";
import 'rxjs/add/operator/map';
import {HttpInterceptor} from "../Interceptor";

@Injectable()
export class DirectoryService {

  private getSpecificCategoryUrl = config.getEnvironmentVariable('endPoint') + 'api/search_service.php?type=directory';
  private getCategoryUrl = config.getEnvironmentVariable('endPoint') + 'api/category.php';
  private headers = config.getHeader();

  constructor(private http: HttpInterceptor) {
  }
// &offer_zipcode=IP24%203LN&offer_zipcode_val=&slider_val=15&question=oui&offer_category=

  getCategory(){

    let authHeader = new Headers();
    return this.http.get(this.getCategoryUrl, { headers: authHeader });
  }

  getSpecificCategory(search){
    let authHeader = new Headers();
    let params = new URLSearchParams();
    params.append('offer_zipcode', search.offer_zipcode);
    params.append('slider_val', search.slider_val);
    params.append('offer_category',search.offer_category);
    let options = new RequestOptions({ headers: authHeader, search: params });
    return this.http.get(this.getSpecificCategoryUrl,options);
  }
}





