import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {DirectoryModal} from "./directory.modal";
import {DirectoryService} from "./directory.service";
import { AlertController } from 'ionic-angular';
import { SingleCategoryPage } from '../../pages/single-category/single-category';
import { PopoverController } from 'ionic-angular';
import { MyPopOverPage } from '../my-category-popover/my-category-popover';
import { DomSanitizer } from '@angular/platform-browser';


/**
 * Generated class for the DirectoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-directory',
  templateUrl: 'directory.html',
})
export class DirectoryPage {
  categoryList = [];
  offerList = [];
  selectedCategoryName;
  private directoryModal = new DirectoryModal;

  constructor(public popoverCtrl: PopoverController, private toastCtrl: ToastController, private alertCtrl: AlertController,
     public navCtrl: NavController,
              public directoryService: DirectoryService, public navParams: NavParams,private sanitizer: DomSanitizer) {
    this.getCategory();
  }

  singleCategory(offerId, offerType) {
    console.log(offerType);
    this.navCtrl.push('SingleCategoryPage', {'offerId': offerId, 'offerType': 'directory'});
  }

  presentAlert(offer) {
    let alert = this.alertCtrl.create({
      title: 'Email',
      subTitle: offer.des.email
    });
    alert.present();
  }

  getCategory() {
    this.directoryService.getCategory().subscribe(response => {
      this.categoryList = response.json();
      // this.childList = response.json().childCategory;
      console.log(this.categoryList);
    }, error => {
      console.log(error);
      // this.error = JSON.parse(error._body);
    });
  }

  getSpecificCategory() {
    this.directoryService.getSpecificCategory(this.directoryModal).subscribe(response => {
      // this.offerList.push(response.json());
      this.offerList = response.json();

      if (this.offerList.length == 0) {
        this.presentToast();
      } else {
        for(var index = 0; index < this.offerList.length; ++index) {
          if(this.offerList[index].lat && this.offerList[index].lng && this.offerList[index].lat != "" && this.offerList[index].lng != "") {
            this.offerList[index].mapURL = this.mapUrl(this.offerList[index].lat, this.offerList[index].lng);
          }
        }
      }
      console.log(this.offerList);
    }, error => {
      console.log(error);
      // this.error = JSON.parse(error._body);
    });
  }

  openWeb(offer) {
    var link = offer.des.website;
    console.log(link.indexOf("http://"), 'rjhfhr');
    if (link.indexOf("http://") == -1) {
      window.open('http://' + offer.des.website);
    } else {
      window.open(offer.des.website);
    }

  }

  // openDialler(offer) {
  //   this.callNumber.callNumber(offer.des.telephone, true)
  //     .then(() => console.log('Launched dialer!'))
  //     .catch(() => console.log('Error launching dialer'));
  // }

  openCall(offer){
    window.open('tel:' + offer.des.telephone, '_system');
  }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'No Data Found!!',
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoryPage');
  }


  ngAfterViewInit() {
    // this.loadMap();
  }

  presentPopover() {
    console.log('fesrf');
    let popover = this.popoverCtrl.create(MyPopOverPage);
    popover.present();
    popover.onDidDismiss((popoverData) => {
      console.log(popoverData);
      if (popoverData) {
        this.directoryModal.offer_category = popoverData.type + "_" +popoverData.id;
        this.selectedCategoryName = popoverData.name;
      }

    })
  }
  mapUrl(lat, lng) {
    var tempURL = "https://www.google.com/maps/embed/v1/place?key=AIzaSyB_0uMeFv4grdPqOr3OI-euMK5gYmY9xGM&q=" + lat + "," + lng;
    return this.sanitizer.bypassSecurityTrustResourceUrl(tempURL);
  }
}
