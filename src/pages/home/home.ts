import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {HomeService} from "./home.service";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  constructor(public navCtrl: NavController, public homeService:HomeService) {

  }

login(){
    this.navCtrl.setRoot('CategoryPage');
}
forgotPassword(){
  this.navCtrl.push('ForgotPasswordPage');
}
}
