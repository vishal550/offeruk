import {Component} from '@angular/core';
import { NavController,ViewController} from 'ionic-angular';
import {Events} from 'ionic-angular';
import {CategoryService} from "../category/category.service";

@Component({
  selector: 'page-popup',
  templateUrl: 'my-category-popover.html'
})
export class MyPopOverPage {
  selectedUser;
  categoryList = [];
  constructor(public categoryService : CategoryService,public navCtrl: NavController,private viewCtrl: ViewController, public events: Events){
 this.getCategory();
  }

  getCategory(){
    this.categoryService.getCategory().subscribe(response => {
      this.categoryList = response.json();
      // this.childList = response.json().childCategory;
      console.log(this.categoryList);
      // console.log(this.childList);
    }, error => {
      console.log(error);
      // this.error = JSON.parse(error._body);
    });
  }


  selectCategory(id?,name?, type?) {
    console.log(id,name, type);
    var data = {
      'id':id,
      'name':name,
      'type': type
    };
    this.viewCtrl.dismiss(data);
  }

}
