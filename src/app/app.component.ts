import { Component,ViewChild } from '@angular/core';
import { Nav,Platform , Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import {CategoryPage} from "../pages/category/category";
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage:any = 'CategoryPage';
  pages: Array<{title: string, component: any}>;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
              private events: Events) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    this.pages = [
      // {title: 'Home', component: FarmInfoPage},
      {title: 'Home', component: 'CategoryPage'},
      {title: 'About', component: 'AboutPage'},
      // {title: 'Register', component: 'RegistrationPage'},
      {title: 'Directory Search', component: 'DirectoryPage'},
      {title: 'Contact Us', component: 'ContactUsPage'},
      // {title: 'Login', component: HomePage}
      // {title: 'Logout', component: LogoutPage},
    ];
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }


  onMenuOpen(event) {
    this.events.publish('sidebar:open');
  }

  onMenuClose(event) {
    this.events.publish('sidebar:close');
  }
}

