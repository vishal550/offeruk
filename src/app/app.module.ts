import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import {Geolocation} from '@ionic-native/geolocation';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import {HomeService} from '../pages/home/home.service';
import {HttpInterceptor} from "../pages/Interceptor";
import {HttpModule} from "@angular/http";
import {CategoryService} from "../pages/category/category.service";
import {DirectoryService} from "../pages/directory/directory.service";
import {ContactService} from "../pages/contact-us/contact-us.service";
import {SingleCategoryService} from "../pages/single-category/single-category.service";
import {MyPopOverPage} from "../pages/my-category-popover/my-category-popover";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    MyPopOverPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    MyPopOverPage

],
  providers: [
    StatusBar,
    SplashScreen,
    HomeService,
    HttpInterceptor,
    CategoryService,
    DirectoryService,
    ContactService,
    SingleCategoryService,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  ]
})
export class AppModule {}
